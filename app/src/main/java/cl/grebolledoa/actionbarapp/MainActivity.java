package cl.grebolledoa.actionbarapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //obtenemos nuestra toolbar desde los recursos de la app
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //cambiamos la toolbar por defecto del la actividad
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //le asignamos a la toolbar nuestro menu de opciones
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //obtenemos el id del menu al cual se le hizo click
        int id = item.getItemId();
        //preguntamos si es el action_settings y no hacemos ninguna acción
        if(id == R.id.action_settings){
            return true;
        }
        //preguntamos si el acerca de y hacemos una acción
        if(id == R.id.acercaDe){
            lanzarAcercaDe();
            return true;
        }
        //en caso de no tener una acción por defecto retornamos la implementación de la clase padre
        return super.onOptionsItemSelected(item);
    }

    public void lanzarAcercaDe(){
        //creamos un intent que nos permite movernos a una nueva actividad
        Intent intent = new Intent(this, OtherActivity.class);
        //lanzamos nuestra nueva actividad por medio del intent
        startActivity(intent);
    }
}