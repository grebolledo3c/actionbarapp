package cl.grebolledoa.actionbarapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.MenuItem;

public class OtherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);

        //obtenemos nuestra toolbar desde los recursos de la app
        Toolbar toolbar = findViewById(R.id.toolbar);
        //cambiamos la toolbar por defecto del la actividad
        setSupportActionBar(toolbar);

        //validamos que la actividad tenga una toolbar
        if(getSupportActionBar() != null){
            //agregamos el boton que no permitira volver a la actividad anterior
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //en caso del que el item seleccionado sea android.R.id.home
        // cerraremos esta actividad y volveremos a la activdad anterior
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}